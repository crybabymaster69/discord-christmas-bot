.PHONY: run debug

run:
	unset HATTER_DEBUG
	venv/bin/python -m christmas.main

debug:
	HATTER_DEBUG=1 venv/bin/python -m christmas.main
