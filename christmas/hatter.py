import animeface
import face_recognition
import PIL.Image
import os

def debug(*args):
    if os.getenv("HATTER_DEBUG") is not None:
        print(*args)

def put_hat(input_file, output_file, hat_file="santa_hat.png"):
    print("Putting hat!")

    all_locations = []

    # feels like bad code oh well
    image = PIL.Image.open(input_file)
    if max(image.size[0], image.size[1]) > 2000:
        f = 2000 / max(image.size[0], image.size[1])
        image.resize((round(image.size[0] * f), round(image.size[1] * f))).save(input_file, format='JPEG')

    image = face_recognition.api.load_image_file(input_file)
    locations = face_recognition.face_locations(image)
    print(f'face_recognition locations: {locations}')
    for location in locations:
        top, right, bottom, left = location
        x = left
        y = top
        width = right - left
        height = bottom - top
        all_locations.append((x, y, width, height))

    image = PIL.Image.open(input_file)
    locations = animeface.detect(image)
    print(f'animeface locations: {locations}')

    hat_base = PIL.Image.open(hat_file)

    for location in locations:
        pos = location.face.pos
        all_locations.append((pos.x, pos.y, pos.width, pos.height))

    for (x, y, width, _height) in all_locations:
        scale = 1.5
        size = (round(width * scale),
                round(hat_base.size[1] * width // hat_base.size[0] * scale))
        hat = hat_base.resize(size)

        image.paste(hat,
                    (round(x - size[0] * (scale-1) / 4.0),
                     round(y - size[1] * 0.8)),
                    mask=hat)

    image.convert('RGB').save(output_file)

