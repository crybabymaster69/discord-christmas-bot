from . import hatter
import discord
from discord.ext import commands
import os
import random

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix="!",
                   activity=discord.Game(name='Christmas!'),
                   intents=intents)

async def get_guilds():
    ans = ['guilds']
    async for guild in bot.fetch_guilds(limit=11):
        ans.append(guild.name)
    if len(ans) >= 11:
        ans[-1] = '...'
    return '\n'.join(ans)

@bot.command()
async def ping(ctx):
    await ctx.send('holly jolly pong!')

@bot.command()
async def pong(ctx):
    await ctx.send('and a merry ping to you!')

@bot.command()
async def isitchristmasyet(ctx):
    from datetime import datetime
    now = datetime.utcnow()
    response = "Yes! Merry Christmas!" if now.day == 25 and now.month == 12 else "No"
    await ctx.send(response)

@bot.command()
async def isit2023yet(ctx):
    from datetime import datetime, timezone, timedelta
    now = datetime.now()
    tz = timezone(timedelta(hours=-3))
    now = now.astimezone(tz)
    print(f"now = {now}")
    response = "Happy New Year!" if now.year == 2023 else "Just a little longer..."
    await ctx.send(response)

# ChatGPT-generated :)
RESPONSES = [
    "Ho ho ho! It's Christmas time!",
    "Jingle all the way to the tree!",
    "Deck the halls with boughs of holly!",
    "Merry Christmas, everyone!",
    "It's the most wonderful time of the year!",
    "The stockings are hung by the chimney with care!",
    "All I want for Christmas is you!",
    "It's beginning to look a lot like Christmas!",
    "Let's get into the holiday spirit!",
    "Wrap it up, it's time for presents!",
    "Don your Santa hats, it's time to celebrate!",
    "Wear your most festive hat to the holiday party!",
    "Top off your holiday outfit with a jolly hat!",
    "Put on your hat and join the carolers!",
    "Don't forget to wear your reindeer antlers!",
    "Top off your ugly Christmas sweater with a elf hat!",
    "Rock your holiday beanie with pride!",
    "Put on your holiday headband and get in the spirit!",
    "A red and green hat is a must for Christmas!",
    "Don't forget to wear your holiday top hat to the gala!",
]

def random_response():
    return random.choice(RESPONSES)

async def put_on_head(ctx, thing):
    # I sure hope there isn't an XSS attack in here
    os.system(f"curl -o input '{ctx.message.attachments[0].url}'")

    # Put the hats!
    hatter.put_hat('input', 'merry-xmas.jpg', hat_file=thing)

    # Send response
    await ctx.message.reply(random_response(),
                            file=discord.File('merry-xmas.jpg'))

    # Delete generated files
    if os.path.exists('input'):
        os.remove('input')
    if os.path.exists('merry-xmas.jpg'):
        os.remove('merry-xmas.jpg')

@bot.command()
async def hat(ctx):
    if len(ctx.message.attachments) == 0:
        return await ctx.send("Please include an image in your !hat message!")
    return await put_on_head(ctx, 'santa_hat.png')

@bot.command()
async def yukata(ctx):
    if len(ctx.message.attachments) == 0:
        return await ctx.send("Please include an image in your !yukata message!")
    return await put_on_head(ctx, 'kimono.png')

@bot.listen('on_message')
async def message_handler(message: discord.Message):
    if os.getenv('HATTER_DEBUG') is not None:
        print(f"Got message <{message.content}> [{message.attachments}]")

    mentions = (os.getenv('MENTIONBACK') or "").split(' ')
    if message.content.strip() in mentions:
        await message.reply(message.author.mention)

if __name__ == '__main__':

    if not os.getenv('HATTER_TOKEN'):
        print("os.environ does not contain HATTER_TOKEN!")
        exit(1)

    print("The Merry Hatter is running!")
    bot.run(os.getenv('HATTER_TOKEN'))
